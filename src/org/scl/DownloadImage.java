package org.scl;

import java.io.IOException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

public class DownloadImage extends AsyncTask<URL, Void, Bitmap> {

	private ImageView tmp_image;
	
	public DownloadImage(ImageView image)
	{
		tmp_image = image;
	}
	
	protected Bitmap doInBackground(URL... urls)
	{
		URL url = urls[0];
		Bitmap bp = null;
		
		try
		{
			bp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
			
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return bp;
	}
	
	protected void onPostExecute(Bitmap bmp)
	{
		tmp_image.setImageBitmap(bmp);
	}

}
