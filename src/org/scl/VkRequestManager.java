package org.scl;

import android.os.AsyncTask;
import android.widget.Toast;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ExecutionException;

class VkApiMethods {
    class Users {
        static final String SEARCH = "users.search";
        static final String SEARCH_XML = "users.search.xml";
    }

    class Wall {
        static final String GET = "wall.get";
        static final String GET_XML = "wall.get.xml";

        static final String POST = "wall.post";
    }
}

class RequestParams extends HashMap<String, String> {

    @Override
    public String toString() {
        String res = "";

        String[] keys = new String[this.size()];
        Set<String> k = this.keySet();
        k.toArray(keys);
        for (String s: keys) {
            if (this.containsKey(s)) {
                try {
                    res = res + "&" + s + "=" + URLEncoder.encode(this.get(s), "utf8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        return res;
    }
}

class GetRequest extends AsyncTask<String, Void, String> {

    StringBuilder m = new StringBuilder();
    HttpURLConnection l = null;
    URL url = null;
    BufferedReader br = null;

    @Override
    protected String doInBackground(String... strings) {
        try
        {
            url = new URL(strings[0]);
            l = (HttpsURLConnection) url.openConnection();
            l.setDoInput(true);
            l.setConnectTimeout(3000);
            l.setUseCaches(false);
            br = new BufferedReader(new InputStreamReader(l.getInputStream(),"utf8"));
            String s;
            try {
                while ((s = br.readLine()) != null)
                    m.append(s);
                br.close();
            } catch (IOException m) {
                m.printStackTrace();
            }
        } catch(NullPointerException e) {
            e.printStackTrace();
        } catch (IOException r) {
            r.printStackTrace();
        }
        return m.toString();
    }
}

public class VkRequestManager {
    private static final String request = "https://api.vk.com/method/";
    private static String api_version = "5.21";

    public static void setApiVersion(String api) {
        api_version = api;
    }

    public static String query(String method, String params) {
        String s = null;

        if (params == null || method == null || method.equals(""))
            return "";

        try {
            GetRequest gr = new GetRequest();
            s = gr.execute(request + method + "?v=" + api_version + params).get();
        } catch (InterruptedException e){
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static String query(String method, String params, String token) {
        return query(method, params + "&access_token=" + token);
    }
}
